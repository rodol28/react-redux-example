import React, { useState } from "react";
import Page from "./page";

import { connect } from "react-redux";
import findSuggestions from "../../redux/actions/findSuggestions";

function AppBar(props) {
  const [text, setText] = useState("");
  const [suggestions, setSuggestions] = useState(props.suggestions);

  const onChangeText = text => {
    setText(text);
    props.findSuggestions(text);
  };
  
  const onChangeSelection = text => {};

  return (
    <Page
      text={text}
      suggestions={suggestions}
      onChangeText={onChangeText}
      onChangeSelection={onChangeSelection}
    />
  );
}

const mapStateToProps = state => {
  return {
    suggestions: state.suggestions,
  };
};

const mapDispathToProps = {
  findSuggestions,
};

export default connect(mapStateToProps, mapDispathToProps)(AppBar);
