import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "components/AppBar";

export default function Page() {
  return (
    <>
      <CssBaseline />
      <AppBar />
    </>
  );
}
