import React from "react";
import Page from "./page";

import { connect } from "react-redux";

function Results(props) {
  const { suggestions } = props;

  return <Page />;
}

const mapStateToProps = state => {
  return {
    suggestions: state.suggestions,
  };
};

// const wrapper = connect(mapStateToProps);
// const component = wrapper(Results);

export default connect(mapStateToProps)(Results);
