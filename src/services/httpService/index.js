import { get } from "./getHttp";
import { post } from "./postHttp";

export function serviceHttp({ apiURL, method, data }) {
  switch (method) {
    case "POST":
      return post(apiURL, data);
    case "GET":
      return get(apiURL);
    default:
      return new Promise((resolve, reject) =>
        reject("You must define the method")
      );
  }
}
