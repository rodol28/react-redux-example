import { isTokenInLocalStorage, createOptionsHttp } from "./helpers";

export function post(url, data) {
  if (url !== undefined && data !== undefined) {
    const hasToken = isTokenInLocalStorage();
    const options = createOptionsHttp("POST", hasToken, data);

    return fetch(url, options)
      .then(response => response.json())
      .then(res => res)
      .catch(err => {
        console.error(err);
        return {};
      });
  } else {
    return new Promise((resolve, reject) => {
      reject("You must define the url and the body of the request");
    });
  }
}
