import { TOKEN_KEY_NAME } from "../const";

const HAS_TOKEN = true;
const DOESNT_HAS_TOKEN = false;

export function isTokenInLocalStorage() {
  const token = localStorage.getItem(TOKEN_KEY_NAME);
  return token ? HAS_TOKEN : DOESNT_HAS_TOKEN;
}
