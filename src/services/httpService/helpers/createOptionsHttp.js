import { TOKEN_KEY_NAME } from "../const";

export function createOptionsHttp(method, hasToken, data) {
  const options = {
    method,
    headers: {
      "Content-Type": "application/json",
    },
  };

  if (hasToken) {
    options.headers.Authorization = localStorage.getItem(TOKEN_KEY_NAME);
  }

  if (data) {
    options.body = JSON.stringify(data);
  }

  return options;
}
