import items from "data/items";
import { type as findSuggestions} from "../actions/findSuggestions";

const defaultState = [];

export default function reducer(state = defaultState, { type, payload }) {
  switch (type) {
    case findSuggestions: {
      const regex = new RegExp(`^${payload}`, "i");
      return items.filter(item => regex.test(item.title));
    }
    default:
      return state;
  }
}
