import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import Results from "components/Results";
import Details from "components/Details";

import { Provider } from "react-redux";
import store from './redux/store'

import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

const Root = (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/results" component={Results} />
        <Route path="/details/:itemId" component={Details} />
        <Redirect from="/" to="/results" />
      </Switch>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(Root, document.getElementById("root"));
